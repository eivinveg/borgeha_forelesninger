# Moduler
# En fil som slutter på .py, og som inneholder funksjoner. Men ikke kall til dem.
# Brukes slik:
import minmodul
print(minmodul.legg_sammen(2,3))
# Vi kan bruke masse eksisterende moduler for ting (kjemi, fysikk, geografi, AI, 
# bildemanipulerin, alt mulig.) 
# Vi kan også lage dem selv! Men ikke kall ting det samme som eksisterende nøkkeluttrykk

# Funksjoner:
# De kan returnere spesifikke ting, eller de kan gjøre en jobb og så ikke returnere noe 
# vi trenger. Funksjoner fyller ulike behov. Noen må ha parametre, andre ikke. Og du kan lage
# dem selv. Men ikke kall dem det samme som eksisterende ting, da kan du overskygge det gamle.

# Vi var også innom måten variable som er definert inni en funksjon lever sitt liv kun der.
# Viste såvidt at en kan definere en variabel som 'global', og da kan en endre på den. Men
# dette er ikke spesielt lurt. Det er bedre å endre verdier gjennom retur.
