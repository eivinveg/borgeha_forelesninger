import numpy as np
a = np.array([[1,2,3,4,5],[3,4,5,6,7]])
b = np.array([[2,4,6,8,9],[6,8,10,12,13]])
print(f'Matrise a:\n{a}')
print(f'Matrise b:\n{b}')
if a.shape == b.shape:
    print(f'\nSummen av matrisene a og b blir \n{a+b}\n')
